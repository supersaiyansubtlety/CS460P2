#ifndef SYN_H
#define SYN_H

#include <iostream>
#include <fstream>
#include "LexicalAnalyzer.h"

using namespace std;

class SyntacticalAnalyzer 
{
    public:
	SyntacticalAnalyzer (char * filename);
	~SyntacticalAnalyzer ();
    private:
	LexicalAnalyzer * lex;
	token_type token;
    ofstream p2OutFile;
    ///<program> -> LPAREN_T <define> LPAREN_T <more_defines> EOF_T
    int program ();
    ///<more_defines> -> <define> LPAREN_T <more_defines>; -> IDENT_T <stmt_list> RPAREN_T
    int more_defines ();
    ///<define> -> DEFINE_T LPAREN_T IDENT_T <param_list> RPAREN_T <stmt> <stmt_list> RPAREN_T; -> {}
    int define ();
    ///<stmt_list> -> <stmt> <stmt_list>
    int stmt_list ();
    ///<stmt> -> <literal>; -> IDENT_T; -> LPAREN_T <action> RPAREN_T
    int stmt ();
    ///<literal> -> NUMLIT_T; -> STRLIT_T; -> SQUOTE_T <quoted_lit>
    int literal ();
    ///<quoted_lit> -> <any_other_token>
    int quoted_lit ();
    ///<more_tokens> -> <any_other_token> <more_tokens>; -> {}
    int more_tokens ();
    ///<param_list> -> IDENT_T <param_list>; -> {}
    int param_list ();
    ///<else_part> -> <stmt>; -> {}
    int else_part ();
    ///<stmt_pair> -> LPAREN_T <stmt_pair_body>; -> {}
    int stmt_pair ();
    ///<stmt_pair_body> -> <stmt> <stmt> RPAREN_T <stmt_pair>; -> ELSE_T <stmt> RPAREN_T
    int stmt_pair_body ();
    ///<action> ->IF_T <stmt> <stmt> <else_part>COND_T LPAREN_T <stmt_pair_body>LISTOP_T <stmt>CONS_T <stmt> <stmt>AND_T <stmt_list>OR_T <stmt_list>NOT_T <stmt>NUMBERP_T <stmt>LISTP_T <stmt>ZEROP_T <stmt>NULLP_T <stmt>STRINGP_T <stmt>PLUS_T <stmt_list>MINUS_T <stmt> <stmt_list>DIV_T <stmt> <stmt_list>MULT_T <stmt_list>MODULO_T <stmt> <stmt>ROUND_T <stmt>EQUALTO_T <stmt_list>GT_T <stmt_list>LT_T <stmt_list>GTE_T <stmt_list>LTE_T <stmt_list>IDENT_T <stmt_list>DISPLAY_T <stmt>NEWLINE_T
    int action ();
    ///<any_other_token> -> LPAREN_T <more_tokens> RPAREN_T; -> IDENT_T; -> NUMLIT_T; -> STRLIT_T; -> CONS_T; -> IF_T; -> DISPLAY_T; -> NEWLINE_T; -> LISTOP_T; -> AND_T; -> OR_T; -> NOT_T; -> DEFINE_T; -> NUMBERP_T; -> LISTP_T; -> ZEROP_T; -> NULLP_T; -> STRINGP_T; -> PLUS_T; -> MINUS_T; -> DIV_T; -> MULT_T; -> MODULO_T; -> ROUND_T; -> EQUALTO_T; -> GT_T; -> LT_T; -> GTE_T; -> LTE_T; -> SQUOTE_T <any_other_token>; -> COND_T; -> ELSE_T
    int any_other_token ();
};
	
#endif
