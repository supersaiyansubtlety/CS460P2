/*******************************************************************************
* Assignment: Project 2 - Syntactic Analyzer for Scheme to C++ Translator      *
* Author: Dr. Watts                                                            *
* Date: Fall 2018                                                              *
* File: Project2.cpp                                                           *
*                                                                              *
* Description: This file contains the                                          *
*******************************************************************************/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include "SetLimits.h"
#include "SyntacticalAnalyzer.h"
using namespace std;

int main (int argc, char * argv[])
{
	if (argc < 2)
	{
		printf ("format: %s <filename>\n", argv[0]);
		exit (1);
	}
/*
	SetLimits ();
*/
	string fileName = argv[1];
	int periodLoc = fileName.find('.');
	if (fileName[periodLoc+1] == 's' && fileName[periodLoc+2] == 's')
	{
	    SyntacticalAnalyzer parse (argv[1]);
	}
	else
	  {
	    cout << "Given file name does not have .ss extension." << endl;
	    cout << "Exiting program." << endl;
	    exit (1);
	  }

	return 0;
}
