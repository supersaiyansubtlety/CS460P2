/*******************************************************************************
* Assignment: Taunt Generator Language Syntactical Analyzer                    *
* Author:Caleb Yost                                                            *
* Date: Fall 2018                                                              *
* File: present_real6.cpp                                                            *
*                                                                              *
* Description: This file contains the Syntactial Analyzer function for the     *
	       non-terminal symbol "article".                                  *
*******************************************************************************/

#include "Syn.h"

int Syn::present_rel ()
{
	int errors = 0; 
	if (ct == "monarch"|| ct=="first-born")
	  {
	    // applying Production Rule 14
	    errors +=present_person();
	    errors += present_verb();
	  }
	else
	{
		lex->ReportError ("'monarch' or 'first-born' expected; '" + ct + "' found.");
		errors++;
	}
	return errors;
} 
