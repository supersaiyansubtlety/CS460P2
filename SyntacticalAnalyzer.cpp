#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "SyntacticalAnalyzer.h"
#include "Rules.cpp"

using namespace std;

SyntacticalAnalyzer::SyntacticalAnalyzer (char * filename)
{
	lex = new LexicalAnalyzer (filename);
	token_type t;
	string stFileName = string(filename);
	int periodLoc = stFileName.find('.');
	string p2OutName = stFileName.substr(0, periodLoc) + ".p2";
	p2OutFile.open(p2OutName);

	token = lex-> GetToken();
	int errors = program ();
}

SyntacticalAnalyzer::~SyntacticalAnalyzer ()
{
	delete lex;
}
