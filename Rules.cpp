//
//  Rules.cpp
//  P2
//
//  Created by Will Lucic on 11/19/18.
//  Copyright © 2018 Team V. All rights reserved.
//

#include "SyntacticalAnalyzer.h"

///<program> -> LPAREN_T <define> LPAREN_T <more_defines> EOF_T
int SyntacticalAnalyzer::program ()
{
    // applying rule 1
    p2OutFile << "Entering Program function; current token is: " + lex->GetTokenName(token) + ", lexeme: " + lex->GetLexeme() << endl;
    p2OutFile << "Using Rule 1" << endl;
    int errors = 0;
    if (token == LPAREN_T)
    {
        token = lex->GetToken();
        errors += define();
        if(token == LPAREN_T)
        {
            token = lex->GetToken();
            errors += more_defines();
            if (token != EOF_T)
            {
                errors++;
                lex->ReportError("End of file 'EOF_T' expected; '" + lex->GetTokenName(token) + "' found.");
                lex->ReportError("Proceeding as though EOF_T found instead and applying rule 1: <program>");
            }
        }
        else
        {
            errors++;
            lex->ReportError("Terminal: '(' with Token Type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
            lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 1: <program>");
            //token = lex->GetToken();
            errors += more_defines();
            if (token != EOF_T)
            {
                errors++;
                lex->ReportError("End of file 'EOF_T' expected; '" + lex->GetTokenName(token) + "' found.");
                lex->ReportError("Proceeding as though EOF_T found instead and applying rule 1: <program>");
            }
        }
    }
    else
    {
        errors++;
        lex->ReportError("Terminal: '(' with Token Type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
        lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 1: <program>");
        //token = lex->GetToken();
        errors += define();
        if(token == LPAREN_T)
        {
            token = lex->GetToken();
            errors += more_defines();
            if (token != EOF_T)
            {
                errors++;
                lex->ReportError("End of file 'EOF_T' expected; '" + lex->GetTokenName(token) + "' found.");
                lex->ReportError("Proceeding as though EOF_T found instead and applying rule 1: <program>");
            }
        }
        else
        {
            errors++;
            lex->ReportError("Terminal: '(' with Token Type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
            lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 1: <program>");
            //token = lex->GetToken();
            errors += more_defines();
            if (token != EOF_T)
            {
                errors++;
                lex->ReportError("End of file 'EOF_T' expected; '" + lex->GetTokenName(token) + "' found.");
                lex->ReportError("Proceeding as though EOF_T found instead and applying rule 1: <program>");
            }
        }
    }

    p2OutFile << "Exiting Program function; current token is: " + lex->GetTokenName(token) << endl;
    return errors++;

}

///<more_defines> -> <define> LPAREN_T <more_defines>
///<more_defines> -> IDENT_T <stmt_list> RPAREN_T
int SyntacticalAnalyzer::more_defines ()
{
    p2OutFile << "Entering more_defines function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;
    
    if(token == IDENT_T)
    {
        p2OutFile << "Using Rule 3" << endl;
        
        token = lex->GetToken();
        errors += stmt_list();
        token = lex->GetToken();
    }
    else if (token != EOF_T)
    {
        p2OutFile << "Using Rule 2" << endl;
        errors += define();
        
        if (token == LPAREN_T)
        {
            token = lex->GetToken();
            errors += more_defines();
        }
        else
        {
            lex->ReportError("Terminal: '(' with Token Type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
            lex->ReportError("Proceeding as though LPAREN_T found instead. ");
            errors += more_defines();
        }
    }

    p2OutFile << "Exiting more_defines function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<define> -> DEFINE_T LPAREN_T IDENT_T <param_list> RPAREN_T <stmt> <stmt_list> RPAREN_T
int SyntacticalAnalyzer::define ()
{
   p2OutFile << "Entering define function; current token is: " + lex->GetTokenName(token) + ", lexeme: " + lex->GetLexeme() << endl;
   p2OutFile << "Using Rule 4" << endl;
   int errors = 0;

   // expected defines
   if (token == DEFINE_T)
   {
       token = lex->GetToken();
       if(token == LPAREN_T)
       {
           token = lex->GetToken();
           if (token == IDENT_T)
           {
             token = lex->GetToken();
             errors += param_list();
             if (token == RPAREN_T)
             {
               token = lex->GetToken();
               errors+=stmt();
               errors+=stmt_list();
               if (token == RPAREN_T)
               {
                 token = lex->GetToken();
               }
               else{
                 errors++;
                 lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
                 lex->ReportError("Proceeding as though RPAREN_T found instead and applying rule 4: <define>");
               }
             }
             else{
               errors++;
               lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
               lex->ReportError("Proceeding as though RPAREN_T found instead and applying rule 4: <define>");
             }
            }
            else {
              errors++;
              lex->ReportError("Token type: 'IDENT_T' expected; '" + lex->GetTokenName(token) + "' found.");
              lex->ReportError("Proceeding as though IDENT_T found instead and applying rule 4: <define>");
               token = lex->GetToken();
               errors += param_list();
               if (token == RPAREN_T)
               {
                   token = lex->GetToken();
                   errors+=stmt();
                   errors+=stmt_list();
                   if (token == RPAREN_T)
                   {
                       token = lex->GetToken();
                   }
                   else{
                       errors++;
                       lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
                       lex->ReportError("Proceeding as though RPAREN_T found instead and applying rule 4: <define>");
                   }
               }
               else{
                   errors++;
                   lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
                   lex->ReportError("Proceeding as though RPAREN_T found instead and applying rule 4: <define>");
               }
            }
          }
          else {
            errors++;
            lex->ReportError("Token type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
            lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 4: <define>");
       }
     }
     else
         {
             errors++;
             lex->ReportError("Token type: 'DEFINE_T'expected; '" + lex->GetTokenName(token) + "' found.");
             lex->ReportError("Proceeding as though DEFINE_T found instead and applying rule 4: <define>");
             token = lex->GetToken();
             errors += define();
         }

    p2OutFile << "Exiting define function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

                        
///<stmt_list> -> <stmt> <stmt_list>
///<stmt_list> -> {}
int SyntacticalAnalyzer::stmt_list ()
{
    p2OutFile << "Entering stmt_list function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;
    
    if (token == NUMLIT_T || token ==  STRLIT_T || token == SQUOTE_T || token == IDENT_T || token == LPAREN_T)
    {// firsts of stmt: call statement
        p2OutFile << "Using Rule 5" << endl;
        errors += stmt();
        errors += stmt_list();
    }
    else if (token == RPAREN_T)
    {// follows of stmt_list: lambda, call more_defines which is where RPAREN_T came from
        p2OutFile << "Using Rule 6" << endl;
    }
    else
    {//else error
        //assume they're missing an RPAREN_T
        errors++;
        lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found and applying Rule 6: <stmt_list> -> {}");
    }

    p2OutFile << "Exiting stmt_list function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<stmt> -> <literal>
///<stmt> -> IDENT_T
///<stmt> -> LPAREN_T <action> RPAREN_T
int SyntacticalAnalyzer::stmt ()
{
    p2OutFile << "Entering stmt function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;

    if(token == IDENT_T)
    {
        p2OutFile << "Using Rule 8" << endl;
        token = lex->GetToken();
    }
    else if(token == LPAREN_T)
    {
        p2OutFile << "Using Rule 9" << endl;
        token = lex->GetToken();
        errors += action();

        if(token != RPAREN_T)
        {
            lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. using a rule");
        }
        token = lex->GetToken();
    }
    else
    {
        p2OutFile << "Using Rule 7" << endl;
        errors += literal();
    }

    p2OutFile << "Exiting stmt function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<literal> -> NUMLIT_T
///<literal> -> STRLIT_T
///<literal> -> SQUOTE_T <quoted_lit>
int SyntacticalAnalyzer::literal ()
{
    p2OutFile << "Entering literal function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;


    if(token == NUMLIT_T)
    {
        p2OutFile << "Using Rule 10" << endl;
        token = lex->GetToken();
    }
    else if(token == STRLIT_T)
    {
        p2OutFile << "Using Rule 11" << endl;
        token = lex->GetToken();
    }
    else if(token == SQUOTE_T)
    {
        p2OutFile << "Using Rule 12" << endl;
        token = lex->GetToken();
        errors += quoted_lit();
    }
    else
    {
        lex->ReportError("Terminal: number, string, or ''' with Token Type: 'NUMLIT_T', 'STRLIT_T', or 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though NUMLIT_T found. ");
    }

    p2OutFile << "Exiting literal function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<quoted_lit> -> <any_other_token>
int SyntacticalAnalyzer::quoted_lit ()
{
    p2OutFile << "Entering quoted_lit function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    p2OutFile << "Using Rule 13" << endl;
    int errors = any_other_token();

    p2OutFile << "Exiting quoted_lit function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<more_tokens> -> <any_other_token> <more_tokens>
///<more_tokens> -> {}
int SyntacticalAnalyzer::more_tokens ()
{
    p2OutFile << "Entering more_tokens function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;

    if
    (
            token == LPAREN_T  || token == IDENT_T   || token == NUMLIT_T  || token == STRLIT_T
         || token == CONS_T    || token == IF_T      || token == DISPLAY_T || token == NEWLINE_T
         || token == LISTOP_T  || token == AND_T     || token == OR_T      || token == NOT_T
         || token == DEFINE_T  || token == NUMBERP_T || token == LISTP_T   || token == ZEROP_T
         || token == NULLP_T   || token == STRINGP_T || token == PLUS_T    || token == MINUS_T
         || token == DIV_T     || token == MULT_T    || token == MODULO_T  || token == ROUND_T
         || token == EQUALTO_T || token == GT_T      || token == LT_T      || token == GTE_T
         || token == LTE_T     || token == SQUOTE_T  || token == COND_T    || token == ELSE_T
    )
    {//firsts of any_other_token: call any_other_token, more_tokens (recursive)
        p2OutFile << "Using Rule 14" << endl;
        errors += any_other_token();
        errors += more_tokens();
    }
    else if(token == RPAREN_T)
    {//follows of more_tokens, from: <any_other_token> -> LPAREN_T <more_tokens> RPAREN_T
     // lambda
        p2OutFile << "Using Rule 15" << endl;
    }
    else
    {
        lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
    }

    p2OutFile << "Exiting more_tokens function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<param_list> -> IDENT_T <param_list>
///<param_list> -> {}
int SyntacticalAnalyzer::param_list ()
{
    p2OutFile << "Entering param_list function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;
    if(token == IDENT_T)
    {
        p2OutFile << "Using Rule 16" << endl;
        token = lex->GetToken();
        errors += param_list();
    }
    else if(token == RPAREN_T)
    {
        p2OutFile << "Using Rule 17" << endl;
    }
    else
    {
        lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
    }

    p2OutFile << "Exiting param_list function; current token is: " << lex->GetTokenName(token) << endl;
    return errors;
}

///<else_part> -> <stmt>
///<else_part> -> {}
int SyntacticalAnalyzer::else_part ()
{
    p2OutFile << "Entering else_part function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;

    if(token == IDENT_T || token == LPAREN_T || token == NUMLIT_T || token == STRLIT_T || token == SQUOTE_T)
    {//firsts of else_part (from fists of stmt): call statment
        p2OutFile << "Using Rule 18" << endl;
        errors += stmt();
    }
    else if(token == RPAREN_T)
    {//follows of else_part: lambda
        p2OutFile << "Using Rule 19" << endl;
    }
    else
    {
        lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
    }

    p2OutFile << "Exiting else_part function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<stmt_pair> -> LPAREN_T <stmt_pair_body>
///<stmt_pair> -> {}
int SyntacticalAnalyzer::stmt_pair ()
{
    p2OutFile << "Entering stmt_pair function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;

    if(token == LPAREN_T)
    {//firsts of stmt_pair: call statment
        p2OutFile << "Using Rule 20" << endl;
        token = lex->GetToken();
        errors += stmt_pair_body();
    }
    else if(token == RPAREN_T)
    {//follows of stmt_pair_body: lambda
        p2OutFile << "Using Rule 21" << endl;
    }
    else
    {
        lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
    }
    
    p2OutFile << "Exiting stmt_pair function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<stmt_pair_body> -> <stmt> <stmt> RPAREN_T <stmt_pair>
///<stmt_pair_body> -> ELSE_T <stmt> RPAREN_T
int SyntacticalAnalyzer::stmt_pair_body ()
{
    p2OutFile << "Entering stmt_pair_body function; current token is: " << lex->GetTokenName(token) << ", lexeme: " << lex->GetLexeme() << endl;
    int errors = 0;
    
    if (token == ELSE_T)
    {
        p2OutFile << "Using Rule 23" << endl;
        token = lex->GetToken();
        errors += stmt();

        if(token != RPAREN_T)
        {
            lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
        }
        else
        {
            token = lex->GetToken();
        }
    }
    else
    {
        p2OutFile << "Using Rule 22" << endl;
        errors += stmt();
        errors += stmt();

        if(token == RPAREN_T)
        {
            token = lex->GetToken();
            errors += stmt_pair();
        }
        else
        {
            lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
        }
    }
    
    p2OutFile << "Exiting stmt_pair_body function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<action> -> IF_T <stmt> <stmt> <else_part>
///<action> -> COND_T LPAREN_T <stmt_pair_body>
///<action> -> LISTOP_T <stmt>
///<action> -> CONS_T <stmt> <stmt>
///<action> -> AND_T <stmt_list>
///<action> -> OR_T <stmt_list>
///<action> -> NOT_T <stmt>
///<action> -> NUMBERP_T <stmt>
///<action> -> LISTP_T <stmt>
///<action> -> ZEROP_T <stmt>
///<action> -> NULLP_T <stmt>
///<action> -> STRINGP_T <stmt>
///<action> -> PLUS_T <stmt_list>
///<action> -> MINUS_T <stmt> <stmt_list>
///<action> -> DIV_T <stmt> <stmt_list>
///<action> -> MULT_T <stmt_list>
///<action> -> MODULO_T <stmt> <stmt>
///<action> -> ROUND_T <stmt>
///<action> -> EQUALTO_T <stmt_list>
///<action> -> GT_T <stmt_list>
///<action> -> LT_T <stmt_list>
///<action> -> GTE_T <stmt_list>
///<action> -> LTE_T <stmt_list>
///<action> -> IDENT_T <stmt_list>
///<action> -> DISPLAY_T <stmt>
///<action> -> NEWLINE_T
int SyntacticalAnalyzer::action ()
{
    p2OutFile << "Entering action function; current token is: " + lex->GetTokenName(token) + ", lexeme: " + lex->GetLexeme() << endl;
    int errors = 0;
    if (token == IF_T)
    {
        p2OutFile << "Using Rule 24" << endl;
        token = lex->GetToken();
        errors += stmt();
        errors += stmt();
        errors += else_part();
    }
    else if (token == COND_T)
    {
        p2OutFile << "Using Rule 25" << endl;
        token = lex->GetToken();
        if (token == LPAREN_T)
        {
            token = lex->GetToken();
            errors += stmt_pair_body();
        }
        else
        {
            errors++;
            lex->ReportError("Terminal: '(' with Token Type: 'LPAREN_T' expected; '" + lex->GetTokenName(token) + "' found.");
            lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 25: <action>");
            errors += stmt_pair_body();
        }

    }
    else if (token == LISTOP_T)
    {
        p2OutFile << "Using Rule 26" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == CONS_T)
    {
        p2OutFile << "Using Rule 27" << endl;
        token = lex->GetToken();
        errors += stmt();
        errors += stmt();
    }
    else if (token == AND_T)
    {
        p2OutFile << "Using Rule 28" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    else if (token == OR_T)
    {
        p2OutFile << "Using Rule 29" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    else if (token == NOT_T)
    {
        p2OutFile << "Using Rule 30" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == NUMBERP_T)
    {
        p2OutFile << "Using Rule 31" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == LISTP_T)
    {
        p2OutFile << "Using Rule 32" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == ZEROP_T)
    {
        p2OutFile << "Using Rule 33" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == NULLP_T)
    {
        p2OutFile << "Using Rule 34" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == STRINGP_T)
    {
        p2OutFile << "Using Rule 35" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == PLUS_T)
    {
        p2OutFile << "Using Rule 36" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    else if (token == MINUS_T)
    {
        p2OutFile << "Using Rule 37" << endl;
        token = lex->GetToken();
        errors += stmt();
        errors += stmt_list();
    }
    else if (token == DIV_T)
    {
        p2OutFile << "Using Rule 38" << endl;
        token = lex->GetToken();
        errors += stmt();
        errors += stmt_list();
    }
    else if (token == MULT_T)
    {
        p2OutFile << "Using Rule 39" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    else if (token == MODULO_T)
    {
        p2OutFile << "Using Rule 40" << endl;
        token = lex->GetToken();
        errors += stmt();
        errors += stmt();
    }
    else if (token == ROUND_T)
    {
        p2OutFile << "Using Rule 41" << endl;
        token = lex->GetToken();
        errors += stmt();
    }
    else if (token == EQUALTO_T)
    {
        p2OutFile << "Using Rule 42" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    else if (token == GT_T)
    {
        p2OutFile << "Using Rule 43" << endl;
        token = lex->GetToken();
        errors+=stmt_list();
    }
    else if (token == LT_T)
    {
        p2OutFile << "Using Rule 44" << endl;
        token = lex->GetToken();
        errors+=stmt_list();
    }
    else if (token == GTE_T)
    {
        p2OutFile << "Using Rule 45" << endl;
        token = lex->GetToken();
        errors+=stmt_list();
    }
    else if (token == LTE_T)
    {
        p2OutFile << "Using Rule 46" << endl;
        token = lex->GetToken();
        errors+=stmt_list();
    }
    else if (token == IDENT_T)
    {
        p2OutFile << "Using Rule 47" << endl;
        token = lex->GetToken();
        errors+=stmt_list();
    }
    else if (token == DISPLAY_T)
    {
        p2OutFile << "Using Rule 48" << endl;
        token = lex->GetToken();
        errors+=stmt();
    }
    else if (token == NEWLINE_T)
    {
        p2OutFile << "Using Rule 49" << endl;
        token = lex->GetToken();
    }
    else
    {
        errors++;
        lex->ReportError("<action> terminal character expected;" + lex->GetTokenName(token) + "' found.");
        lex->ReportError("Proceeding as IDENT_T found instead and applying rule 47: <action>");
        p2OutFile << "Using Rule 47" << endl;
        token = lex->GetToken();
        errors += stmt_list();
    }
    p2OutFile << "Exiting action function; current token is: " + lex->GetTokenName(token) << endl;
    return errors;
}

///<any_other_token> -> LPAREN_T <more_tokens> RPAREN_T
///<any_other_token> -> IDENT_T
///<any_other_token> -> NUMLIT_T
///<any_other_token> -> STRLIT_T
///<any_other_token> -> CONS_T
///<any_other_token> -> IF_T
///<any_other_token> -> DISPLAY_T
///<any_other_token> -> NEWLINE_T
///<any_other_token> -> LISTOP_T
///<any_other_token> -> AND_T
///<any_other_token> -> OR_T
///<any_other_token> -> NOT_T
///<any_other_token> -> DEFINE_T
///<any_other_token> -> NUMBERP_T
///<any_other_token> -> LISTP_T
///<any_other_token> -> ZEROP_T
///<any_other_token> -> NULLP_T
///<any_other_token> -> STRINGP_T
///<any_other_token> -> PLUS_T
///<any_other_token> -> MINUS_T
///<any_other_token> -> DIV_T
///<any_other_token> -> MULT_T
///<any_other_token> -> MODULO_T
///<any_other_token> -> ROUND_T
///<any_other_token> -> EQUALTO_T
///<any_other_token> -> GT_T
///<any_other_token> -> LT_T
///<any_other_token> -> GTE_T
///<any_other_token> -> LTE_T
///<any_other_token> -> SQUOTE_T <any_other_token>
///<any_other_token> -> COND_T
///<any_other_token> -> ELSE_T
int SyntacticalAnalyzer::any_other_token () {
   p2OutFile << "Entering any_other_token function; current token is: " + lex->GetTokenName(token) + ", lexeme: " + lex->GetLexeme() << endl;
   int errors = 0;
    // Rule 50
    if (token == LPAREN_T) {
        p2OutFile << "Using Rule 50" << endl;
        token = lex->GetToken();
        errors += more_tokens();
        if (token == RPAREN_T) {
            token = lex->GetToken();
        }
        else {
            lex->ReportError("Terminal: ')' with Token Type: 'RPAREN_T' expected; '" + lex->GetTokenName(token) + "' found. Proceeding as though RPAREN_T found. ");
        }
        // Rule 79
    }
    else if (token == SQUOTE_T) {
        p2OutFile << "Using Rule 79" << endl;
        token = lex->GetToken();
        errors+=any_other_token();
    }
    // Whats left
    // else if (token == "IDENT_T" || token == "NUMLIT_T" || token == "STRLIT_T" || token == "CONS_T" ||
    //          token == "IF_T" || token == "DISPLAY_T" || token == "NEWLINE_T" || token == "LISTOP_T" || token == "AND_T" ||
    //          token == "OR_T" || token == "NOT_T" || token == "DEFINE_T" || token == "NUMBERP_T" || token == "SYMBOLP_T" ||
    //          token == "LISTP_T" || token == "ZEROP_T" || token == "NULLP_T" || token == "STRINGP_T" || token == "PLUS_T" ||
    //          token == "MINUS_T" || token == "DIV_T" || token == "MULT_T" || token == "MODULO_T" || token == "ROUND_T"
    //          token == "EQUALTO_T" || token == "GT_T" || token == "LT_T" || token == "GTE_T" || token == "LTE_T" ||
    //          token == "COND_T" || token == "ELSE_T"){
    //     if (token == "IDENT_T"){
    //       p2OutFile << "Using Rule 51" << endl;
    //       token = lex->GetToken();

      else if (token == NUMLIT_T) {
        p2OutFile << "Using Rule 52" << endl;
        token = lex->GetToken();
      }
      else if (token == STRLIT_T) {
        p2OutFile << "Using Rule 53" << endl;
        token = lex->GetToken();
      }
      else if (token == CONS_T) {
        p2OutFile << "Using Rule 54" << endl;
        token = lex->GetToken();
      }
      else if (token == IF_T) {
        p2OutFile << "Using Rule 55" << endl;
        token = lex->GetToken();
      }
      else if (token == DISPLAY_T) {
        p2OutFile << "Using Rule 56" << endl;
        token = lex->GetToken();
      }
      else if (token == NEWLINE_T) {
        p2OutFile << "Using Rule 57" << endl;
        token = lex->GetToken();
      }
      else if (token == LISTOP_T) {
        p2OutFile << "Using Rule 58" << endl;
        token = lex->GetToken();
      }
      else if (token == AND_T) {
        p2OutFile << "Using Rule 59" << endl;
        token = lex->GetToken();
      }
      else if (token == OR_T) {
        p2OutFile << "Using Rule 60" << endl;
        token = lex->GetToken();
      }
      else if (token == NOT_T) {
        p2OutFile << "Using Rule 61" << endl;
        token = lex->GetToken();
      }
      else if (token == DEFINE_T) {
        p2OutFile << "Using Rule 62" << endl;
        token = lex->GetToken();
      }
      else if (token == NUMBERP_T) {
        p2OutFile << "Using Rule 63" << endl;
        token = lex->GetToken();
      }
      else if (token == LISTP_T) {
        p2OutFile << "Using Rule 64" << endl;
        token = lex->GetToken();
      }
      else if (token == ZEROP_T) {
        p2OutFile << "Using Rule 65" << endl;
        token = lex->GetToken();
      }
      else if (token == NULLP_T) {
        p2OutFile << "Using Rule 66" << endl;
        token = lex->GetToken();
      }
      else if (token == STRINGP_T) {
        p2OutFile << "Using Rule 67" << endl;
        token = lex->GetToken();
        }
      else if (token == PLUS_T) {
        p2OutFile << "Using Rule 68" << endl;
        token = lex->GetToken();
      }
      else if (token == MINUS_T) {
        p2OutFile << "Using Rule 69" << endl;
        token = lex->GetToken();
      }
      else if (token == DIV_T) {
        p2OutFile << "Using Rule 70" << endl;
        token = lex->GetToken();
      }
      else if (token == MULT_T) {
        p2OutFile << "Using Rule 71" << endl;
        token = lex->GetToken();
      }
      else if (token == MODULO_T) {
        p2OutFile << "Using Rule 72" << endl;
        token = lex->GetToken();
      }
      else if (token == ROUND_T) {
        p2OutFile << "Using Rule 73" << endl;
        token = lex->GetToken();
      }
      else if (token == EQUALTO_T) {
        p2OutFile << "Using Rule 74" << endl;
        token = lex->GetToken();
      }
      else if (token == GT_T) {
        p2OutFile << "Using Rule 75" << endl;
        token = lex->GetToken();
      }
      else if (token == LT_T) {
        p2OutFile << "Using Rule 76" << endl;
        token = lex->GetToken();
      }
      else if (token == GTE_T) {
        p2OutFile << "Using Rule 77" << endl;
        token = lex->GetToken();
      }
      else if (token == LTE_T) {
        p2OutFile << "Using Rule 78" << endl;
        token = lex->GetToken();
      }
      else if (token == COND_T) {
        p2OutFile << "Using Rule 80" << endl;
        token = lex->GetToken();
      }
      else if (token == ELSE_T) {
        p2OutFile << "Using Rule 81" << endl;
        token = lex->GetToken();
      }
      else{
        errors++;
        lex->ReportError("<any_other_token> terminal character expected;" + lex->GetTokenName(token) + "' found.");
        lex->ReportError("Proceeding as though LPAREN_T found instead and applying rule 50: <any_other_token>");
        p2OutFile << "Using Rule 50" << endl;
        //token = lex->GetToken();
        errors += any_other_token();
      }
      p2OutFile << "Exiting any_other_token function; current token is: " + lex->GetTokenName(token) << endl;
      return errors;
}
